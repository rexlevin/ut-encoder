# 介绍

这是一个用于编码转换的utools插件

目前有：

1. navtive和ascii互转

2. native和utf-8互转

3. AES/DES加解密

4. BASE64编码/解码

5. MD5、SHA加密

utools官网：[https://u.tools/](https://u.tools/)

utools插件开发：[https://u.tools/docs/developer/welcome.html](https://u.tools/docs/developer/welcome.html)

项目使用了webpack4、bootstrap3，这是我自学webpack的第一个小项目。

开发环境：ubuntu

适用环境：linux、windows

mac没试过。

# todo

- [ ] 增加二维码生成/解码

- [ ] 增加md5、sha、hamc等

# 参与开发

1. clone项目`git clone https://gitee.com/rexlevin/ut-encoder.git`

2. 进入项目运行`npm install`安装开发环境，npm会根据`package.json`文件安装项目依赖

3. 开始开发

# 下载完整的程序包

utools已经提供插件中心，encoder插件已经可以从插件中心下载。

也可以从这里[百度网盘](https://pan.baidu.com/s/1BnCjgf5GkRT9sl5wbtDRBQ)（提取码：m93e）下载已经打包好的upx插件包使用。

# 更新日志

2020-05-28 0.0.6

- 插件发布有问题，改下版本下重发

2020-02-25 0.0.5

- 增加md5、sha加密

2019-08-24 0.0.4

- 增加BASE64编码/解码

2019-08-05 0.0.3

- 增加AES/DES加解密

2019-07-27  0.0.1 0.0.2

- navtive和ascii互转、native和utf-8互转

# 一点情况

发现使用bootstrap4时标签页样式不正确，似乎是4里面不再支持标签页`nav-tab`，得自己用`nav-pill`来实现。所以最后还是使用了bootstrap3。

# 记录开发过程中使用命令：

```shell
npm -y init # 初始化项目，我是建好目录再init的，所以使用了-y参数
npm install --save-dev webpack webpack-cli
npm install --save-dev css-loader style-loader file-loader html-webpack-plugin
npm install --save-dev jquery bootstrap@3 popper.js  # bootstrap需要使用到jquery，还依赖了popper.js
npm install --save-dev vue
npm install --save-dev crypto-js  # 加解密使用
vi .gitignore # 配置忽略目录
npm run dev # 开发模式打包
npm run dist # 发布模式打包
```