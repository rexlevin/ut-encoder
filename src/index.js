
// const bscss = require("bootstrap/dist/css/bootstrap.min.css");
// const $ = require("jquery/dist/jquery.min");   // 在webpack.config.js中已经有jquery的配置，这里可以不用再引入
// const bs = require("bootstrap/dist/js/bootstrap.min.js");
// const popper = require("popper.js/dist/popper.min.js");

// 以下写法同样ok
// import bscss from "bootstrap/dist/css/bootstrap.min.css";   // 这样写同时申明了bootstrap样式的实例bcss
import "bootstrap/dist/css/bootstrap.min.css";
// import $ from "jquery";   // 在webpack.config.js中已经有jquery的配置，这里可以不用再引入
import "bootstrap/dist/js/bootstrap.min.js";
import Vue from "vue/dist/vue.min.js";
import "./assets/css/index.css";
import "./assets/js/n2a.js";
import "./assets/js/n2u.js";
import "./assets/js/ent.js";
import "./assets/js/hash.js";
import "./assets/js/b64.js";
import "./assets/icon/converter.png";

// $("body").append("<div>hello world</div>");

new Vue ({
  el: "#algorithm",
  data: {
    aes: "AES",
    des: "DES"
  }
});