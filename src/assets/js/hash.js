import CryptoJS from "crypto-js"; // 引入加解密工具类

$(function (){
  /*
   * 32位md5
   * 16位加密就是从32位MD5散列中把中间16位提取出来，如：
   * 16位：45c567dc9f701502
   * 32位：11d6467b45c567dc9f70150236ba81ec
   */
  $("#btn_md5").on("click", function () {
    $("#txt_hden").val(CryptoJS.MD5($("#txt_hen").val()).toString());
  });

  // 以下是sha相关
  $("#btn_sha1").on("click", function() {
    $("#txt_hden").val(CryptoJS.SHA1($("#txt_hen").val()).toString());
  });
  $("#btn_sha224").on("click", function() {
    $("#txt_hden").val(CryptoJS.SHA224($("#txt_hen").val()).toString());
  });
  $("#btn_sha256").on("click", function() {
    $("#txt_hden").val(CryptoJS.SHA256($("#txt_hen").val()).toString());
  });
  $("#btn_sha384").on("click", function() {
    $("#txt_hden").val(CryptoJS.SHA384($("#txt_hen").val()).toString());
  });
  $("#btn_sha512").on("click", function() {
    $("#txt_hden").val(CryptoJS.SHA512($("#txt_hen").val()).toString());
  });

  // 以下是hmac相关
  $(".hah").on("click", function() {
    $("#txt_hden").height("300px");
  });
  $("#btn_hmacsha1").on("click", function() {
    // CryptoJS.HmacSHA1("Message", "Key")
    $("#txt_hden").val(CryptoJS.HmacSHA1($("#txt_hen").val(), "asdfasdfasdf"));
  });
});