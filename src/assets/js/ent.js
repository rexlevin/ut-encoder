import CryptoJS from "crypto-js"; // 引入加解密工具类

$(function (){
  // 加密
  $("#ent").on("click",function() {
    // var plaintext = $("#entxt").val(), entkey = $("#entkey").val(), ciphertext = "";
    switch ($("#algorithm").val()) {
      case "0":
        return;
        break;
      case "aes":
        $("#dentxt").val(CryptoJS.AES.encrypt($("#entxt").val(), $("#entkey").val()));
        break;
      case "des":
          $("#dentxt").val(CryptoJS.DES.encrypt($("#entxt").val(), $("#entkey").val()));
        break;
      default:
        return;
        break;
    }
  });

  // 解密
  $("#dent").on("click", function () {
    switch ($("#algorithm").val()) {
      case "0":
        return;
        break;
      case "aes":
        $("#entxt").val(CryptoJS.AES.decrypt($("#dentxt").val(), $("#entkey").val()).toString(CryptoJS.enc.Utf8));
        break;
      case "des":
          $("#entxt").val(CryptoJS.DES.decrypt($("#dentxt").val(), $("#entkey").val()).toString(CryptoJS.enc.Utf8));
        break;
      default:
        return;
        break;
    }
  });
});