import CryptoJS from "crypto-js"; // 引入加解密工具类

$(function (){
  // 加密
  $("#bent").on("click",function() {
    // var plaintext = $("#entxt").val(), entkey = $("#entkey").val(), ciphertext = "";
    var str=CryptoJS.enc.Utf8.parse($("#bentxt").val());
    var base64=CryptoJS.enc.Base64.stringify(str);
    $("#bdentxt").val(base64);
  });

  // 解密
  $("#bdent").on("click", function () {
    var words  = CryptoJS.enc.Base64.parse($("#bdentxt").val());
    $("#bentxt").val(words.toString(CryptoJS.enc.Utf8));
  });
});