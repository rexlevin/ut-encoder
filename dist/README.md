这是一个用于编码转换的utools插件

![](https://img03.sogoucdn.com/app/a/100520146/3aa08fdeb4d116e7b96c10bc4ac629b9)

目前已有：

1. native和ascii互转（2019-07-27  0.0.1）

2. native和utf-8互转（2019-07-27  0.0.2）

3. AES/DES加解密（2019-08-05 0.0.3）

4. BASE64编码/解码（2019-08-24 0.0.4）

5. md5、sha加密（2020-02-25 0.0.5）

项目地址：[https://gitee.com/rexlevin/ut-encoder](https://gitee.com/rexlevin/ut-encoder)

开发环境：ubuntu

适用环境：linux、windows

mac没试过。